﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionPlatform : MonoBehaviour, IInteractable
{

    private SpriteRenderer _platformImage;

    private void Start()
    {
        _platformImage = transform.GetComponent<SpriteRenderer>();
    }

    public void Interact()
    {
        _platformImage.color = Random.ColorHSV();
    }
}
