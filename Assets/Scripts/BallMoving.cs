﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class BallMoving : MonoBehaviour
{

    [SerializeField] private float _jumpForce = 10;
    [SerializeField] private float _leanSpeed = 2;
    
    private CircleCollider2D _ballCollider;
    private Vector2 _moveForceVector;
    private float _windForce = 2;
    private float _gravity;
    private float _rotationSpeed = 10f;

    public void SetupBall(float gravity)
    {
        _gravity = gravity / 10;//idk how to make smooth moving like in video reference without this magic number. Shame on me.
        _ballCollider = GetComponent<CircleCollider2D>();
    }
    
    private void Update()
    {
        BallHitInteractionLogic();
        
        RotateBall();
        
        SetBallNewPosition();
    }
    private void BallHitInteractionLogic()
    {
        var hittedObject = Physics2D.CircleCast(transform.position, _ballCollider.radius + 0.1f, Vector2.zero);
        if (hittedObject)
        {
            _rotationSpeed = Random.Range(1, 10);
            _moveForceVector = GetJumpVector(hittedObject.point);
            var objectInteraction = hittedObject.transform.GetComponent<IInteractable>();
            objectInteraction?.Interact();
        }
    }

    private Vector3 _rotationSide = Vector3.forward;
    private void RotateBall()
    {
        transform.rotation *= Quaternion.Euler(_rotationSpeed * _rotationSide);
    }

    private Vector2 GetJumpVector(Vector2 hittedObjectPos)
    {
        var position = transform.position;
        var jumpDirection = (new Vector2(position.x, position.y) - hittedObjectPos).normalized;
        var _oldXMoveVector = _moveForceVector.x;
        return jumpDirection * _jumpForce + new Vector2(_oldXMoveVector,0);
    }

    private void SetBallNewPosition()
    {
        var newPos = GetNewMoveForce();
        transform.position = newPos;
    }

    private Vector2 GetNewMoveForce()
    {
        var newXForce = Mathf.Lerp(_moveForceVector.x, GetLeanForce(),_windForce * Time.deltaTime);
        var newYForce = _moveForceVector.y - _gravity * Time.deltaTime;
        _moveForceVector = new Vector2(newXForce, newYForce);
        return transform.position + new Vector3(_moveForceVector.x, _moveForceVector.y, 0);
    }

    private float GetLeanForce()
    {
        if (!Input.GetMouseButton(0))
            return 0;
        
        var currentPos = transform.position;
        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var isLeanToRight = mousePos.x - currentPos.x > 0;
        var leanForceValue = isLeanToRight ? _leanSpeed : -_leanSpeed;
        return leanForceValue;
    }

}
