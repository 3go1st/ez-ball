﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChoosePlanetButtonUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _buttonName;
    [SerializeField] private Button _buttonScript;

    public void ButtonSetup(PlanetsScriptableObject.PlanetConf planetConf)
    {
        _buttonName.text = planetConf.Name;
        _buttonScript.onClick.RemoveAllListeners();
        _buttonScript.onClick.AddListener(()=> PlanetSelector.instance.LoadPlanet(planetConf));
    }
}
