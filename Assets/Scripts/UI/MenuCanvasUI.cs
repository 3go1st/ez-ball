﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MenuCanvasUI : MonoBehaviour
{
    [FormerlySerializedAs("_allPlanetsConfig")] [FormerlySerializedAs("_planetsConfiguration")] [SerializeField] private PlanetsScriptableObject _planetsScriptableObject;
    [SerializeField] private GameObject _choosePlanetButton;

    [SerializeField] private RectTransform _buttonContainer;
    
    // Start is called before the first frame update
    private void Start()
    {
        RemoveAllButtonsFromContainer();
        InstantiateAllPlanetButtons();
    }

    private void RemoveAllButtonsFromContainer()
    {

        for (int i = 0; i < _buttonContainer.childCount; i++)
        {
            Destroy(_buttonContainer.GetChild(i).gameObject);
        }
    }

    private void InstantiateAllPlanetButtons()
    {
        foreach (var planetConfig in _planetsScriptableObject.PlanetConfs)
        {
            var buttonObj = Instantiate(_choosePlanetButton, _buttonContainer, false);
            buttonObj.GetComponent<ChoosePlanetButtonUI>().ButtonSetup(planetConfig);
        }
    }
}
