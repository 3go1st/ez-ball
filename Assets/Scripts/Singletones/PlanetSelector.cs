﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlanetSelector : MonoBehaviour
{
    private static PlanetSelector _instance;
    public static PlanetSelector instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<PlanetSelector>();
                if (_instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = "~GameManager";
                    _instance = obj.AddComponent<PlanetSelector>();
                    DontDestroyOnLoad(obj);
                    
                    return _instance;
                }
            }

            return _instance;

    
        }
    }


    private static string GAME_SCENE = "GameScene";

    private PlanetsScriptableObject.PlanetConf _currentPlanetConfig;
    public PlanetsScriptableObject.PlanetConf CurrentPlanetConfig => _currentPlanetConfig;

    public void LoadPlanet(PlanetsScriptableObject.PlanetConf planetConf)
    {
        _currentPlanetConfig = planetConf;
        SceneManager.LoadScene(GAME_SCENE);
    }
}
