﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetManager : MonoBehaviour
{
    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private Transform _spawnPoint;

    private PlanetsScriptableObject.PlanetConf _currentPlanetConf;
    
    private void Awake()
    {
        _currentPlanetConf = PlanetSelector.instance.CurrentPlanetConfig;
        Camera.allCameras[0].backgroundColor = _currentPlanetConf.BackgroundColor;
        var obj = Instantiate(_ballPrefab, _spawnPoint);
        obj.GetComponent<BallMoving>().SetupBall(PlanetSelector.instance.CurrentPlanetConfig.Gravity);
    }
}
