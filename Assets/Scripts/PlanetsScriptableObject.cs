﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlanetsConfigScriptableObj", menuName = "Create new Planets Configuration", order = 1)]
public class PlanetsScriptableObject : ScriptableObject
{
    public enum PlanetType
    {
        DefaultPlanet,
        Earth,
        Moon,
        Jupiter
    }

    [SerializeField] private List<PlanetConf> _planetConfs = new List<PlanetConf>();
    public List<PlanetConf> PlanetConfs => _planetConfs;
    
    [Serializable]
    public class PlanetConf
    {
        [SerializeField] private string _name = "Default Planet";
        public string Name => _name;
        
        [SerializeField] private PlanetType _planet;
        public PlanetType Planet => _planet;
        
        [SerializeField] private float _gravity;
        public float Gravity => _gravity;

        [SerializeField] private Color _backgroundColor;
        public Color BackgroundColor => _backgroundColor;
    }
}
